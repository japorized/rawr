import { Command } from "./types.ts";
import { ConflictingFlags, ConflictingSubcommands } from "./exceptions.ts";
import { getFullDefinition } from "./cmd_defn.ts";
import { assert, assertEquals, assertThrows } from "../dev_deps.ts";

const getCmdCopy = (): Command => ({
  name: "git",
  description: "the stupid content tracker",
  flags: [
    {
      name: "version",
      description:
        "Prints the Git suite version that the git program came from.",
      valueType: "boolean",
      short: "v",
      default: false,
    },
    {
      name: "config-env",
      description:
        "Pass by <name=value>, e.g. `--config-env key=val1`. Give configuration variable name.",
      valueType: "string",
      short: "c",
    },
  ],
  subcommands: [
    {
      name: "branch",
      description: "List, create, or delete git branches",
      flags: [
        {
          name: "delete",
          description: "Delete a branch",
          valueType: "string",
          short: "d",
        },
        {
          name: "force",
          description: "Ensure action happens",
          valueType: "boolean",
          short: "f",
        },
        {
          name: "D",
          description: "Shortcut for --delete --force",
          valueType: "string",
          short: "D",
        },
      ],
    },
    {
      name: "commit",
      description: "Record changes to the repository",
      flags: [
        {
          name: "all",
          description: "Record changes for all files",
          valueType: "boolean",
          short: "a",
        },
        {
          name: "help",
          description: "Show a custom help message",
          valueType: "boolean",
          short: "h",
        },
      ],
    },
  ],
});

Deno.test(`${getFullDefinition.name} should automatically insert help flags if missing`, () => {
  const cmd = getCmdCopy();
  const cmdFullDefn = getFullDefinition(cmd);

  // Automatically inserts help flag
  assert(cmdFullDefn.flags?.find((flag) => flag.name === "help"));
  assert(
    cmdFullDefn.subcommands?.find((subcmd) => subcmd.name === "branch")?.flags
      ?.find((flag) => flag.name === "help"),
  );
  assert(
    cmdFullDefn.subcommands?.find((subcmd) => subcmd.name === "commit")?.flags
      ?.find((flag) => flag.name === "help"),
  );

  // Does not override user-set help flag
  assertEquals(
    cmdFullDefn.subcommands?.find((subcmd) => subcmd.name === "commit")?.flags
      ?.find((flag) => flag.name === "help")?.description,
    "Show a custom help message",
  );
});

Deno.test(`${getFullDefinition.name} throws ${ConflictingFlags.name} on duplicate flags within a command`, () => {
  const cmd = getCmdCopy();
  cmd.subcommands?.find((subcmd) => subcmd.name === "branch")?.flags?.push({
    name: "delete",
    description: "Yet another delete flag",
    valueType: "string",
  });

  assertThrows(() => getFullDefinition(cmd), ConflictingFlags);
});

Deno.test(`${getFullDefinition.name} throws ${ConflictingSubcommands.name} on duplicate subcommands within a command`, () => {
  const cmd = getCmdCopy();
  cmd.subcommands?.push({
    name: "branch",
    description: "Do something with git branches",
  });

  assertThrows(() => getFullDefinition(cmd), ConflictingSubcommands);
});
