import { FlagValueType } from "./types.ts";

export class ConflictingFlags extends Error {
  constructor(flagName: string) {
    super(`Flag ${flagName} appeared multiple times!`);
  }
}

export class ConflictingSubcommands extends Error {
  constructor(subcmdName: string) {
    super(`Subcommand ${subcmdName} appeared multiple times!`);
  }
}

export class InvalidChoiceForFlag<T = FlagValueType> extends Error {
  constructor(flagName: string, choice: T, availableChoices: T[]) {
    super(
      `Selection "${choice}" for flag ${flagName} is not available in [${
        availableChoices.join(", ")
      }]`,
    );
  }
}
