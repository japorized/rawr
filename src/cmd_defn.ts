import { Command, CommandDefinition, FlagDefinition } from "./types.ts";
import {
  checkForConflictingFlags,
  checkForConflictingSubcommands,
} from "./_utils.ts";

/**
 * Convert the Command definition into a full definition.
 *
 * This sets down quite a lot of defaults and does various definition
 * checks.
 * In particular, this function checks if there are conflicting long and
 * short flags, as well as subcommand names.
 *
 * This function also automatically appends a help flag at the base
 * command, as well as the subcommands.
 *
 * @remark
 * You may want to use this for debugging purposes.
 */
export function getFullDefinition(cmdDefn: Command): CommandDefinition {
  const flags = (() => {
    if (cmdDefn.flags) {
      const flags = cmdDefn.flags.map<FlagDefinition>((flag) => {
        const { short, ...others } = flag;
        return {
          ...others,
          ...(short ? { short } : {}),
        };
      });
      if (!flags.find((flag) => flag.name === "help")) {
        flags.push({
          name: "help",
          description: "Show help message",
          short: "h",
          valueType: "boolean",
          default: false,
        });
      }

      checkForConflictingFlags(flags);

      return flags;
    }
  })();

  const subcommands = (() => {
    if (cmdDefn.subcommands) {
      checkForConflictingSubcommands(cmdDefn.subcommands);
      return cmdDefn.subcommands.map<CommandDefinition>(getFullDefinition);
    }
  })();

  return {
    name: cmdDefn.name,
    description: cmdDefn.description,
    takesArguments: Boolean(cmdDefn.takesArguments),
    ...(flags ? { flags } : {}),
    ...(subcommands ? { subcommands } : {}),
  };
}
