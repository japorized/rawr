import { Command, FlagDefinition } from "./types.ts";
import { ConflictingFlags, ConflictingSubcommands } from "./exceptions.ts";

export function checkForConflictingFlags(
  flags: FlagDefinition[],
): void {
  // Check if there are conflicting long name flags
  flags.reduce((acc, flag) => {
    if (!acc[flag.name]) acc[flag.name] = 0;

    acc[flag.name]++;

    if (acc[flag.name] > 1) throw new ConflictingFlags(flag.name);

    return acc;
  }, {} as Record<string, number>);

  // Check if there are conflicting short name flags
  flags.reduce((acc, flag) => {
    if (flag.short) {
      if (!acc[flag.short]) acc[flag.short] = 0;

      acc[flag.short]++;

      if (acc[flag.short] > 1) {
        throw `Flag ${flag.short} appeared multiple times!`;
      }
    }

    return acc;
  }, {} as Record<string, number>);
}

export function checkForConflictingSubcommands(
  subcommands: Command[],
): void {
  subcommands.reduce((acc, subcmd) => {
    if (subcmd.name) {
      if (!acc[subcmd.name]) acc[subcmd.name] = 0;

      if (subcmd.flags) {
        checkForConflictingFlags(subcmd.flags);
      }

      if (subcmd.subcommands) {
        checkForConflictingSubcommands(subcmd.subcommands);
      }

      acc[subcmd.name]++;

      if (acc[subcmd.name] > 1) {
        throw new ConflictingSubcommands(subcmd.name);
      }
    }

    return acc;
  }, {} as Record<string, number>);
}
