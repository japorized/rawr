import { Command, FlagDefinition } from "./types.ts";
import {
  checkForConflictingFlags,
  checkForConflictingSubcommands,
} from "./_utils.ts";
import { assertThrows } from "../dev_deps.ts";
import { ConflictingFlags, ConflictingSubcommands } from "./exceptions.ts";

Deno.test(`${checkForConflictingFlags.name} should not throw when there aren't conflicts`, () => {
  const flags: FlagDefinition[] = [
    {
      name: "output-dir",
      description: "Specify the output directory",
      valueType: "string",
      short: "o",
      default: "./outputs/",
    },
    {
      name: "input-dir",
      description: "Specify the input directory",
      valueType: "string",
      short: "i",
    },
  ];

  checkForConflictingFlags(flags);
});

Deno.test(`${checkForConflictingFlags.name} should throw when there are conflicts`, () => {
  const flags: FlagDefinition[] = [
    {
      name: "output-dir",
      description: "Specify the output directory",
      valueType: "string",
      short: "o",
      default: "./outputs/",
    },
    {
      name: "output-dir",
      description: "Specify the output directory again!?",
      valueType: "string",
      short: "o",
    },
  ];

  assertThrows(() => checkForConflictingFlags(flags), ConflictingFlags);
});

Deno.test(`${checkForConflictingSubcommands.name} should not throw when there aren't conflicts`, () => {
  const subcommands: Command[] = [
    {
      name: "commit",
      description: "Commit files",
      takesArguments: false,
      flags: [
        {
          name: "all",
          description: "Commit all files",
          valueType: "boolean",
          short: "a",
          default: false,
        },
      ],
    },
    {
      name: "pull",
      description: "Pull changes from upstream",
      takesArguments: false,
    },
  ];

  checkForConflictingSubcommands(subcommands);
});

Deno.test(`${checkForConflictingSubcommands.name} should throw when there are conflicts`, () => {
  const subcommands: Command[] = [
    {
      name: "commit",
      description: "Commit files",
      takesArguments: false,
      flags: [
        {
          name: "all",
          description: "Commit all files",
          valueType: "boolean",
          short: "a",
          default: false,
        },
      ],
    },
    {
      name: "commit",
      description: "Yet another commit subcommand",
      takesArguments: true,
    },
  ];

  assertThrows(
    () => checkForConflictingSubcommands(subcommands),
    ConflictingSubcommands,
  );
});
