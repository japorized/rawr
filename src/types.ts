type MakePartial<T, U extends keyof T> =
  & Omit<T, U>
  & Partial<Pick<T, U>>;

export type FlagValueType = number | string | boolean;

/**
 * The complete definition for a flag.
 *
 * If you're a library user, you shouldn't need to use this type, and should
 * just use the `Flag` type {@see Flag}
 */
export type FlagDefinition<T = FlagValueType> = {
  /**
   * The name of the flag, which is also the long name of the flag
   */
  name: string;
  /**
   * Description for the flag
   */
  description: string;
  /**
   * Expected value type that the flag will hold
   */
  valueType: "number" | "string" | "boolean";
  /**
   * A fixed list of choices for this flag
   */
  availableChoices?: T[];
  /**
   * A short version of the flag.
   *
   * @example
   * If the name of the flag is `output-dir`, then you can set it as `o`, or `d`.
   */
  short?: string;
  /**
   * The default value this flag will hold.
   */
  default?: T;
};

/**
 * The complete definition for a command
 *
 * If you're a library user, you shouldn't need to use this type explicitly.
 * Use `Command` (@see Command) instead.
 */
export type CommandDefinition = {
  /**
   * Name of the command
   */
  name: string;
  /**
   * Description of the command
   */
  description: string;
  /**
   * Whether this command takes arguments
   */
  takesArguments: boolean;
  /**
   * Flags for this command
   */
  flags?: FlagDefinition[];
  /**
   * Subcommands under this command
   */
  subcommands?: CommandDefinition[];
};

/**
 * Flags are optional parameters that gets passed to the command.
 * You can use a flag for actions like:
 * - changing underlying parameters for your command
 * - optionally perform something extra in a given command
 */
export type Flag<T = FlagValueType> = FlagDefinition<T>;

/**
 * A basic command friendly for the library user.
 */
export type Command =
  & Omit<
    MakePartial<CommandDefinition, "takesArguments">,
    "flags" | "subcommands"
  >
  & {
    /**
     * Flags for this command
     */
    flags?: Flag[];
    /**
     * Subcommands under this comamnd
     */
    subcommands?: Command[];
  };

export type CommandArguments<
  T = Record<string, unknown>,
  U extends string | number | symbol = string | number | symbol,
> = {
  values?: string[];
  flags?: T;
  subcommand?: Record<U, CommandArguments>;
};
