import { stdParse } from "../deps.ts";
import { CommandArguments, CommandDefinition } from "./types.ts";
import { InvalidChoiceForFlag } from "./exceptions.ts";

export function parse<
  T extends Record<string, unknown>,
  U extends string | number | symbol,
>(
  args: string[],
  defn: CommandDefinition,
  cmdConstruct: CommandArguments<T, U>,
): CommandArguments<T, U> {
  const parsed = stdParse(args, {
    stopEarly: true,
    string: defn.flags?.filter((flag) => flag.valueType === "string").map((
      flag,
    ) => flag.name),
    boolean: defn.flags?.filter((flag) => flag.valueType === "boolean").map((
      flag,
    ) => flag.name),
    alias: defn.flags?.reduce(
      (aliasMap, flag) => {
        if (flag.short) {
          if (!aliasMap[flag.short]) {
            aliasMap[flag.short] = flag.name;
          }
        }
        return aliasMap;
      },
      {} as Record<string, string>,
    ),
  });

  if (defn.flags) {
    for (const flag of defn.flags) {
      const flagVal = parsed[flag.name];
      if (flagVal) {
        const flagDefn = defn.flags.find((f) => f.name === flag.name);
        if (flagDefn) {
          if (flagDefn.availableChoices) {
            if (!flagDefn.availableChoices.includes(flagVal)) {
              throw new InvalidChoiceForFlag(
                flag.name,
                flagVal,
                flagDefn.availableChoices,
              );
            }
          }
        }
        if (!cmdConstruct.flags) cmdConstruct.flags = {} as T;

        Reflect.set(cmdConstruct.flags, flag.name, flagVal);
      } else if (flag.default) {
        if (!cmdConstruct.flags) cmdConstruct.flags = {} as T;

        Reflect.set(cmdConstruct.flags, flag.name, flag.default);
      }
    }
  }
  if (defn.subcommands) {
    const firstValue = parsed._[0];
    const subcommandDefn = defn.subcommands.find((subcmd) =>
      subcmd.name === firstValue
    );
    if (subcommandDefn) {
      if (!cmdConstruct.subcommand) {
        cmdConstruct.subcommand = {} as Record<
          U,
          CommandArguments<Record<string, unknown>, U>
        >;
        Reflect.set(cmdConstruct.subcommand, parsed._[0], {});
      }
      parse(
        parsed._.slice(1).map(String),
        subcommandDefn,
        Reflect.get(cmdConstruct.subcommand, parsed._[0]),
      );
    }
  } else {
    if (defn.takesArguments) {
      cmdConstruct.values = parsed._.map(String);
    }
  }

  return cmdConstruct;
}
