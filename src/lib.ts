import {
  Command,
  CommandArguments,
  CommandDefinition,
  FlagDefinition,
} from "./types.ts";
import { parse } from "./parse.ts";
import { getFullDefinition } from "./cmd_defn.ts";

/**
 * Typing for the usage printer
 */
export type UsagePrinter = (cmdDefn: CommandDefinition) => void;

function checkAndShowUsage(
  cmdArgs: CommandArguments,
  defn: CommandDefinition,
  usagePrinter?: UsagePrinter,
) {
  if (cmdArgs.flags?.help) {
    if (usagePrinter) {
      usagePrinter(defn);
    } else {
      showUsage(defn);
    }
    Deno.exit(0);
  }

  if (cmdArgs.subcommand) {
    for (const key of Object.keys(cmdArgs.subcommand)) {
      const subcommand = cmdArgs.subcommand[key];
      if (subcommand.flags?.help) {
        checkAndShowUsage(
          subcommand,
          defn.subcommands!.find((subcmd) =>
            subcmd.name === key
          ) as CommandDefinition,
          usagePrinter,
        );
      }
    }
  }
}

/**
 * The built-in, default usage printer.
 * Code is fugly, and the output is boring. But it gets its job done.
 *
 * If you don't want your `parseArgs` do use this, you can pass your
 * own usage printer in the options.
 *
 * @remarks
 * This function is exported just so that you can use it to show the
 * help message wherever you need to. Do note that you'll need the
 * `getFullDefinition` function to construct the full definition
 * before using this.
 */
export function showUsage(cmdDefn: CommandDefinition): void {
  const usageHint = (() => {
    let hint = "";
    if (cmdDefn.flags?.length && cmdDefn.subcommands?.length) {
      hint += " [FLAGS/SUBCMD]";
    }
    if (cmdDefn.flags?.length) hint += " [FLAGS]";
    if (cmdDefn.subcommands?.length) hint += " [SUBCMD]";

    if (cmdDefn.takesArguments) hint += " <INPUT>";

    return hint;
  })();

  const formatFlagDesc = (flagDefn: FlagDefinition) => {
    let desc = flagDefn.description;
    if (flagDefn.availableChoices) {
      desc += ` ${flagDefn.availableChoices.join(", ")}`;
    }
    if (flagDefn.default) desc += ` [default: ${flagDefn.default}]`;

    return desc;
  };

  const flagUsage = cmdDefn.flags?.reduce((acc, flag) => {
    acc.push([
      `\n  --${flag.name}${flag.short ? `, -${flag.short}` : ""}`,
      formatFlagDesc(flag),
    ]);
    return acc;
  }, [] as [string, string][]);

  const longestFlagName = flagUsage?.reduce((longest, flagMsg) => {
    if (flagMsg[0].length > longest) longest = flagMsg[0].length;
    return longest;
  }, 0) ?? 0;

  const subcommandUsage = cmdDefn.subcommands?.reduce((acc, subcmd) => {
    acc.push([`\n  ${subcmd.name}`, subcmd.description]);
    return acc;
  }, [] as [string, string][]);

  const longestSubcommandName = subcommandUsage?.reduce(
    (longest, subcmdMsg) => {
      if (subcmdMsg[0].length > longest) longest = subcmdMsg[0].length;
      return longest;
    },
    0,
  ) ?? 0;

  const longestArgname = Math.max(longestFlagName, longestSubcommandName);

  const showArg = (args: [string, string][]) =>
    args.map((argMsg) =>
      `${argMsg[0].padEnd(longestArgname, " ")}   ${argMsg[1]}`
    ).join("");

  console.log(`  ${cmdDefn.name}
  ${cmdDefn.description}
  Usage: ${cmdDefn.name}${usageHint}
  ${
    subcommandUsage
      ? `
  SUBCMD:${showArg(subcommandUsage)}\n`
      : ""
  }${
    flagUsage
      ? `
  FLAGS:${showArg(flagUsage)}`
      : ""
  }
  `);
}

/**
 * Parse arguments based on given definition
 * @param  cmdDefn   Definition of the cli.
 * @param  opts      Options with various configurations. Check jsdoc of each property.
 *
 * @returns formatted arguments passed to the command based on the definition
 */
export function parseArgs<
  T extends Record<string, unknown> = Record<string, unknown>,
  U extends string | number | symbol = string | number | symbol,
>(
  cmdDefn: Command,
  opts: {
    /**
     * Turn on debugging mode to get a bit more insight.
     * **Great for development purposes.**
     */
    debug?: boolean;
    /**
     * Override for printing the help message.
     * Provide your own help message printer if you don't like the default.
     */
    usagePrinter?: UsagePrinter;
  } = { debug: false },
): CommandArguments<T, U> {
  const defn = getFullDefinition(cmdDefn);
  if (opts?.debug) console.dir(defn);
  let cmdArgs: CommandArguments<T, U> = {};

  try {
    if (defn.flags?.length === 0 && defn.subcommands?.length === 0) {
      if (defn.takesArguments) cmdArgs.values = Deno.args;
    } else {
      cmdArgs = parse(Deno.args, defn, cmdArgs);
    }
  } catch (error) {
    if (opts?.debug) console.error(error);
    if (opts?.usagePrinter) {
      opts.usagePrinter(defn);
    } else {
      showUsage(defn);
    }
    throw error;
  }

  checkAndShowUsage(cmdArgs, defn, opts?.usagePrinter);

  return cmdArgs as CommandArguments<T, U>;
}
