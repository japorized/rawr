import { Command, parseArgs } from "../mod.ts";

const cmdDefn: Command = {
  name: "basic",
  description: "This is a basic use of rawr",
  flags: [
    {
      name: "loglevel",
      valueType: "string",
      description: "Adjust log level",
      availableChoices: ["quiet", "chatty", "noisy"],
      default: "chatty",
    },
  ],
  subcommands: [
    {
      name: "subcmd",
      takesArguments: true,
      description: "This is a subcmd",
      flags: [
        {
          name: "force",
          description: "Forcefully do nothing",
          valueType: "boolean",
          short: "f",
        },
        {
          name: "file",
          description: "File to process",
          valueType: "string",
          short: "F",
        },
      ],
    },
  ],
};

console.log(parseArgs(cmdDefn));

// You can optionally pass types for flags and subcommand name.
// However, DX is not the best cause of limitations with TypeScript
const x = parseArgs<{ loglevel: string }, "subcmd">(cmdDefn);

x.flags?.loglevel; // Your LSP should tell you that loglevel is of type "string | undefined"
