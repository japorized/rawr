# rawr

Slightly more pleasant interface to setting up cli flags than standard `parse`. Made for [Deno](https://deno.land).

**Important note**:
This is untested software and I'd even go out to say that it's *pre-alpha-level*. I'm writing this simply for the sake of having a utility tool for writing my own clis.
I'm looking at improving it over time as I consume it for myself.

### Use at your own risk

---

## Usage

`parseArgs` is your main interface to getting your arguments nicely formatted based on the definition you provided using an object of type `Command`.

Here's an example output you get from `parseArgs`, where we're leaving out the `Command` object just for simplicity's sake.
It won't make too much sense as a command, but I hope it describes the interface, enough for you to know what's going on.
```bash
$ deno run my-cli.ts --loglevel quiet -afc /path/to/config/file commit -a --message "Some message" other arguments

{
  flags: {
    loglevel: "quiet",
    config: "/path/to/config/file",
    append: true,
    force: true
  },
  subcommand: {
    commit: {
      flags: {
        all: true,
        message: "Some message"
      },
      values: ["other", "arguments"]
    }
  }
}
```

See also [`examples`]('./examples').

---

## Notes

`showUsage` isn't the prettiest. It's just plain text with the laziest of handling at the moment.
You are, however, free to override with your own printer. Just pass your own printer in the `opts` in `parseArgs`.
